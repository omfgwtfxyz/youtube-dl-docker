FROM alpine:latest
RUN apk update \
	&& apk add py-pip ca-certificates \
	&& apk add ffmpeg \
	&& rm -rf /var/cache/apk/* \
	&& pip install youtube-dl
	
RUN apk add --no-cache bash
CMD ["/bin/bash"]